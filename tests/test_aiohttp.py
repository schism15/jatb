import asyncio
import logging
import time

import pytest

from .aiohttp_irc import FakeTwitchIRC

from jatb.bot import JatbBot

async def main():
    twitch_irc = FakeTwitchIRC()
    jatb_bot = JatbBot()

    await asyncio.gather(
        twitch_irc.init_server(),
        jatb_bot.init_bot()
    )

def test_start(caplog, capsys):
    caplog.set_level(logging.DEBUG)
    asyncio.run(main())

    out, _ = capsys.readouterr()
    assert 'has entered the building' in out