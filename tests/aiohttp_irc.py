'''
A script for inspecting requests from an IRC client. After completing
the authentication flow per RFC 1459, print all messages from the client
to console. Based on https://stackoverflow.com/questions/48506460/python-simple-socket-client-server-using-asyncio
'''
import asyncio
import logging
import time

import aiohttp
from aiohttp import web
import yaml

log = logging.getLogger(__name__)

with open('etc/config.yml') as f:
    chat_cfg = yaml.load(f.read(), Loader=yaml.Loader)['CHAT_CONFIG']

class FakeTwitchIRC:
    def __init__(self):
        self._users = [{
            'nick': chat_cfg['NICK'],
            'pass': chat_cfg['ACCESS_TOKEN'],
        }]

    async def _confirm_login_and_join(self, ws, nick: str, channel: str):
        responses = [
            f':tmi.twitch.tv 001 {nick} :Welcome, GLHF!',
            f':tmi.twitch.tv 002 {nick} :Your host is tmi.twitch.tv',
            f':tmi.twitch.tv 003 {nick} :This server is rather new',
            f':tmi.twitch.tv 004 {nick} :-',
            f':tmi.twitch.tv 375 {nick} :-',
            f':tmi.twitch.tv 372 {nick} :You are in a maze of twisty passages, all alike.',
            f':tmi.twitch.tv 376 {nick} :>',
            f':tmi.twitch.tv CAP * ACK :twitch.tv/commands',
            f':tmi.twitch.tv CAP * ACK :twitch.tv/tags',
            f':tmi.twitch.tv CAP * ACK :twitch.tv/membership',
            f':{nick}!{nick}@{nick}.tmi.twitch.tv JOIN {channel}',
            f':{nick}.tmi.twitch.tv 353 {nick} = {channel} :{nick}',
            f':{nick}.tmi.twitch.tv 366 {nick} {channel} :End of /NAMES list',
            f'@badge-info=;badges=;color=;display-name={nick};emote-sets=0,300374282;mod=0;subscriber=0;user-type= :tmi.twitch.tv USERSTATE {channel}',
            f'@emote-only=0;followers-only=-1;r9k=0;rituals=0;room-id=133295608;slow=0;subs-only=0 :tmi.twitch.tv ROOMSTATE {channel}',
        ]

        for msg in responses:
            log.info(msg)
            await ws.send_str(msg)

    async def _is_authenticated(self, user: dict) -> bool:
        for item in self._users:
            if item == user:
                return True


    async def _websocket_handler(self, request):
        session_is_authed = False
        nick = None
        password = None

        ws = web.WebSocketResponse()
        await ws.prepare(request)
        
        async for msg in ws:
            data = msg.data

            if not session_is_authed:
                if password is not None and nick is not None and session_is_authed is False:
                    session_is_authed = await self._is_authenticated({
                        'nick': nick,
                        'pass': password
                    })

                    if session_is_authed:
                        await self._confirm_login_and_join(
                            ws=ws,
                            nick=nick,
                            channel='#schism15'
                        )
            

                if password is None and msg.data.startswith("PASS"):
                    password = msg.data.split()[1].split(':')[1]

                
                if nick is None and msg.data.startswith("NICK"):
                    nick = msg.data.split()[1]



            else:
                # Once authed, log every request from the client to console
                print(msg.type)
                print(msg.data)

        return ws

    async def init_server(self):
        app = web.Application()
        app.add_routes([web.get('/', self._websocket_handler)])
        runner = aiohttp.web.AppRunner(app)
        await runner.setup()
        site = aiohttp.web.TCPSite(runner, host='localhost', port=6667)
        await site.start()


# async def cli_start():
#     twitch_irc = FakeTwitchIRC()
#     await asyncio.gather(twitch_irc.init_server())


# if __name__ == '__main__':
#     asyncio.run(cli_start())