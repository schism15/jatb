import aiohttp
from aiohttp import web
import asyncio

import yaml

from base import BaseComponent

with open('etc/config.yml') as f:
    vcs_cfg = yaml.load(f.read(), Loader=yaml.Loader)['VCS_CONFIG']

class JatbApi(BaseComponent):
    def __init__(self, loop = None):
        self.app = web.Application()

        self.app.add_routes([web.post('/', self.webhook)])
        self._loop = loop

    
    async def start_server(self):
        asyncio.set_event_loop(self._loop)
        runner = aiohttp.web.AppRunner(self.app)
        await runner.setup()
        site = aiohttp.web.TCPSite(runner, host='localhost', port=8080)
        await site.start()

    
    async def webhook(self, request):
        vcs_webhook_token = vcs_cfg.get('WEBHOOK_TOKEN', '')
        token_header = request.headers['x-gitlab-token']

        if not token_header:
            return web.Response(status=401, text='Unauthorized: No token')

        if token_header != vcs_webhook_token:
            return web.Response(status=401, text='Unauthorized: Invalid token')

        payload = await request.json()

        event = {
            'action': 'merge_request_data_received',
            'payload': payload,
        }

        await self.mediator.notify(self, event)
        return web.Response(status=204, text='') 