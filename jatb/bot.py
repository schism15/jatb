import asyncio
import logging

from twitchio import Channel
from twitchio.ext import commands
import yaml

from base import BaseComponent


with open('etc/config.yml') as f:
    chat_cfg = yaml.load(f.read(), Loader=yaml.Loader)['CHAT_CONFIG']


class JatbBot(commands.Bot, BaseComponent):
    def __init__(self, loop = None):
        super().__init__(
            token=chat_cfg['ACCESS_TOKEN'],
            prefix=chat_cfg['PREFIX'],
            initial_channels=chat_cfg['INITIAL_CHANNELS'],
            nick=chat_cfg['NICK'],
        )
        self._channel = None
    
    async def event_ready(self):
        ready_message = f'{self.nick} has entered the building'
        print(ready_message)

    @commands.command()
    async def hello(self, ctx: commands.Context):
        await ctx.send(f'Hello {ctx.author.name}!')

    async def init_bot(self, from_cli: bool = True):
        await self.connect()

        if from_cli:
            await asyncio.Event().wait()
        else:
            await self._connection.wait_until_ready()


    async def send_message(self, payload: dict):
        if not self._channel:
            self._channel = Channel(
                name=chat_cfg['INITIAL_CHANNELS'][0],
                websocket=self._connection,
            )

        event_description = payload['body']['object_attributes']['description']
        url = payload['body']['object_attributes']['url']

        chat_message = (
            f'Commit Message: {event_description} -- '
            f'{url}'
        )

        await self._channel.send(chat_message)


async def cli_start():
    jatb_bot = JatbBot()
    await asyncio.gather(jatb_bot.init_bot(from_cli=True))


if __name__ == '__main__':
    asyncio.run(cli_start())