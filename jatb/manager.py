import asyncio
import logging

from base import BaseMediator
from bot import JatbBot
from api import JatbApi


class JatbManager(BaseMediator):
    def __init__(self) -> None:
        self._bot = None
        self._api = None

    async def main(self, from_cli: bool = False):
        await asyncio.gather(
            self._bot.init_bot(from_cli=from_cli),
            self._api.start_server()
        )

    def init_otb(self, from_cli: bool = False):
        jatb_bot = JatbBot()
        self._bot = jatb_bot
        self._bot.mediator = self

        jatb_api = JatbApi()
        self._api = jatb_api
        self._api.mediator = self

        asyncio.get_event_loop().run_until_complete(self.main(from_cli=from_cli))

    async def notify(self, sender: object, event: dict) -> None:
        '''Receive events from other components and take
        action based on message/data

        :param sender: Component object sending the event
        :type object:
        :param event: Payload from Component object containing message and data
        :type event: dict
        '''
        if event['action'] == 'merge_request_data_received':
            await self._bot.send_message(event['payload'])
