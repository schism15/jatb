import asyncio

from bot import JatbBot
from api import JatbApi
from manager import JatbManager

if __name__ == '__main__':
    jatb_manager = JatbManager()
    jatb_manager.init_otb(from_cli=True)