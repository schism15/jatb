# jatb (Just Another Twitch Bot)

jatb (pronounced jat-bee) is a Twitch bot that is focused on providing utility to live coding streamers. The main thesis behind development is that there are opportunities to better leverage bots to improve the viewer's situational awareness of the coding project, which improves their ability to ramp up on understaning what is going on, which increases their likelihood of staying with a stream. "The Problem with Live Streaming Code" by Ben Awad what he sees as the problems preventing the Live Coding genre from achieving more popularity and the project aims to address number 3 on that list.


An example feature that would address this would be using a bot that automatically pushes a GitLab/GitHub link to merge commits. New viewers can use them to quickly get an at-a-glance look at the most recent work without having to leave the stream.